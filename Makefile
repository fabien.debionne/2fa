# Construit les images Docker pour tous les services définis dans le fichier docker-compose.yml
build:
	docker-compose build

# Démarre les conteneurs pour tous les services définis dans le docker-compose.yml en mode détaché
up:
	docker-compose up -d

# Arrête et supprime tous les conteneurs définis dans le docker-compose.yml
down:
	docker-compose down

# Arrête et supprime tous les conteneurs, et supprime également les images créées par le docker-compose build ou pull
remove:
	docker-compose down --rmi all

# Affiche les logs de tous les conteneurs définis dans le docker-compose.yml
logs:
	docker-compose logs

# Permet d'exécuter "le script"
run_client:
	go run client/main.go client/client.go

generate_and_save_token:
# Utilise curl pour faire une requête GET au serveur d'authentification pour obtenir un token
# La réponse est attendue sous la forme "ID: valeur, Token: valeur"
# Sauvegarde la réponse complète dans un fichier nommé last_token.txt
	curl -s "http://localhost:8080/token" > last_token.txt


verify_token:
# Lit le contenu du fichier last_token.txt pour en extraire l'ID et le token
# Utilise 'cut' pour diviser la chaîne par ',', puis par ':' pour isoler l'ID et le token
# 'xargs' est utilisé pour trimmer les espaces éventuels autour des valeurs extraites
	$(eval ID=$(shell cat last_token.txt | cut -d ',' -f1 | cut -d ':' -f2 | xargs))
	$(eval TOKEN=$(shell cat last_token.txt | cut -d ',' -f2 | cut -d ':' -f2 | xargs))
# Fait une requête curl au serveur d'authentification pour vérifier le token précédemment sauvegardé
# en utilisant l'ID et le token extraits du fichier
	curl -s "http://localhost:8080/verify?id=$(ID)&token=$(TOKEN)"


# Commande pour tester un token inexistant
verify_fake_token:
	curl -s "http://localhost:8080/verify?id=fakeID&token=fakeToken"