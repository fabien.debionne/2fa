package main

import (
	"fmt"
	"io"
	"net/http"
)

// startResourceServer initialise et démarre le serveur de ressources
func startResourceServer() {
	// Définit un gestionnaire pour la route /verify
	http.HandleFunc("/verify", func(w http.ResponseWriter, r *http.Request) {
		// Récupère l'ID et le token depuis les paramètres de la requête
		id := r.URL.Query().Get("id")
		token := r.URL.Query().Get("token")

		// Construit l'URL de vérification pour le serveur d'authentification
		verifyURL := fmt.Sprintf("http://auth-server:8080/verify?id=%s&token=%s", id, token)
		// Envoie une requête au serveur d'authentification pour vérifier le token
		resp, err := http.Get(verifyURL)
		if err != nil {
			// Gère les erreurs lors de la requête de vérification
			fmt.Printf("Error during token verification request to auth-server: %v\n", err)
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
		defer resp.Body.Close()

		// Lit la réponse du serveur d'authentification
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			// Gère les erreurs lors de la lecture de la réponse
			fmt.Printf("Error reading the response body from auth-server: %v\n", err)
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}

		// Transforme le corps de la réponse en chaîne de caractères
		responseMessage := string(body)
		// Selon le code de statut de la réponse, gère les différents cas
		switch resp.StatusCode {
		case http.StatusOK:
			// Si le token est valide, accorde l'accès
			fmt.Fprintf(w, "Access granted")
		case http.StatusUnauthorized, http.StatusGone:
			// Si le token est invalide ou expiré, renvoie le message d'erreur correspondant
			fmt.Fprintf(w, responseMessage)
		default:
			// Pour tout autre code de réponse, renvoie une erreur interne du serveur
			http.Error(w, "Unexpected response from auth server", http.StatusInternalServerError)
		}
	})

	// Affiche un message indiquant que le serveur de ressources a démarré et écoute sur le port 8081
	fmt.Println("Resource Server started on port 8081")
	// Démarre le serveur de ressources et écoute sur le port 8081
	http.ListenAndServe(":8081", nil)
}
