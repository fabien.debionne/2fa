package main

import (
	"bufio"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
)

// promptForAction affiche un message et attend une réponse de l'utilisateur.
// Retourne vrai si l'utilisateur répond "1"
func promptForAction(message string) bool {
	fmt.Println(message)
	scanner := bufio.NewScanner(os.Stdin)
	if scanner.Scan() {
		response := strings.TrimSpace(scanner.Text())
		return response == "1"
	}
	return false
}

// startClient démarre le client
func startClient() {
	// Demande à l'utilisateur s'il veut générer un token
	if !promptForAction("Do you want to generate a token? (1: yes, 2: no)") {
		fmt.Println("Program terminated.")
		return
	}

	// Envoie une requête au serveur d'authentification pour obtenir un token
	resp, err := http.Get("http://localhost:8080/token")
	if err != nil {
		fmt.Println("Error requesting token:", err)
		return
	}
	defer resp.Body.Close()

	id, token := "", ""
	// Si la requête est réussie, lit et affiche la réponse
	if resp.StatusCode == http.StatusOK {
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			fmt.Println("Failed to read response body:", err)
			return
		}
		response := strings.TrimSpace(string(body))
		fmt.Println("Response received:", response)
		// Découpe la réponse pour extraire l'ID et le token
		parts := strings.Split(response, ", ")
		if len(parts) == 2 {
			idPart := strings.Split(parts[0], ": ")
			tokenPart := strings.Split(parts[1], ": ")
			if len(idPart) == 2 && len(tokenPart) == 2 {
				id = strings.TrimSpace(idPart[1])
				token = strings.TrimSpace(tokenPart[1])
				fmt.Printf("ID: %s, Token: %s\n", id, token)
			} else {
				fmt.Println("Unexpected response format")
				return
			}
		} else {
			fmt.Println("Unexpected response format")
			return
		}
	} else {
		fmt.Println("Failed to retrieve token")
		return
	}

	// Affiche l'URL pour vérifier manuellement le token
	fmt.Println("If you wish to manually verify the token, please visit the following URL:")
	verifyURL := fmt.Sprintf("http://localhost:8081/verify?id=%s&token=%s", id, token)
	fmt.Println(verifyURL)

	// Demande si l'utilisateur souhaite vérifier automatiquement le token
	if !promptForAction("Do you want to automatically connect to the resource server with the ID and token? (1: yes, 2: no)") {
		fmt.Println("Manual verification required. Program terminated.")
		return
	}

	// Tente de vérifier automatiquement le token avec le serveur de ressources
	fmt.Printf("Automatically connecting to resource server with the ID: %s and Token: %s\n", id, token)
	resp, err = http.Get(verifyURL)
	if err != nil {
		fmt.Println("Error during verification:", err)
		return
	}
	defer resp.Body.Close()

	responseBody, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Failed to read verification response:", err)
		return
	}
	// Affiche la réponse du serveur de ressources
	fmt.Println("Resource server response:", strings.TrimSpace(string(responseBody)))
}
