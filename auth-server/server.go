package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"time"
)

// User représente un utilisateur avec un token et une date d'expiration
type User struct {
	Token      string
	ValidUntil time.Time
}

// users est une map qui stocke les utilisateurs par leur ID.
var users map[string]User

// generateToken crée un token aléatoire sous forme de chaîne de caractères
func generateToken() string {
	token := fmt.Sprintf("%06d", rand.Intn(1000000))
	return token
}

// generateID crée un ID aléatoire sous forme de chaîne de caractères
func generateID() string {
	id := fmt.Sprintf("%03d", rand.Intn(1000))
	return id
}

// startAuthServer démarre le serveur d'authentification
func startAuthServer() {
	users = make(map[string]User) // Initialisation de la map des utilisateurs

	// Gestionnaire pour la route /token qui génère un nouveau token et un ID pour un utilisateur
	http.HandleFunc("/token", func(w http.ResponseWriter, r *http.Request) {
		token := generateToken()
		id := generateID()
		users[id] = User{
			Token:      token,
			ValidUntil: time.Now().Add(30 * time.Second), // Le token expire après 30 secondes
		}
		response := fmt.Sprintf("ID: %s, Token: %s\n", id, token)
		w.Write([]byte(response))
		fmt.Printf("Token generated: %s for ID: %s\n", token, id)
	})

	// Gestionnaire pour la route /verify qui vérifie si un token est valide pour un ID donné
	http.HandleFunc("/verify", func(w http.ResponseWriter, r *http.Request) {
		id := r.URL.Query().Get("id")
		token := r.URL.Query().Get("token")

		user, ok := users[id]
		if !ok {
			w.WriteHeader(http.StatusUnauthorized) // 401
			fmt.Fprintf(w, "Unknown ID")
			return
		}
		if user.Token != token {
			w.WriteHeader(http.StatusUnauthorized) // 401
			fmt.Fprintf(w, "Mismatched token for given ID")
			return
		}
		if time.Now().After(user.ValidUntil) {
			// delete(users, id)
			w.WriteHeader(http.StatusGone) // 410
			fmt.Fprintf(w, "Token expired")
			return
		}
		w.WriteHeader(http.StatusOK) // 200
		fmt.Fprintf(w, "Token valid")
	})

	fmt.Println("Auth Server started on port 8080")
	http.ListenAndServe(":8080", nil)
}
