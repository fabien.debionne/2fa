
# Projet d'Authentification 2FA avec Go et Docker

## Description

Ce projet illustre la mise en œuvre d'un système d'authentification à deux facteurs (2FA). 
Il comprend trois composants principaux : un serveur d'authentification (`auth-server`), un serveur de ressources (`resource-server`) et un client (`client`).

## Prérequis

- Docker doit être installé.
- Go doit être installé.

## Structure du Projet

```
.
├── Makefile
├── README.md
├── auth-server
│   ├── Dockerfile
│   ├── go.mod
│   ├── main.go
│   └── server.go
├── client
│   ├── Dockerfile
│   ├── client.go
│   ├── go.mod
│   └── main.go
├── docker-compose.yml
└── resource-server
    ├── Dockerfile
    ├── go.mod
    ├── main.go
    └── server.go
```

## Configuration et Installation

1. **Cloner le dépôt :**

    ```bash
    git clone [URL_DU_DEPOT]
    cd [NOM_DU_REPO]
    ```

2. **Construire les images Docker :**

    Utilisez le `Makefile` fourni pour construire les images nécessaires :

    ```bash
    make build
    ```

## Démarrage du Projet

Pour démarrer tous les services, utilisez :

```bash
make up
```

Cela lancera le serveur d'authentification, le serveur de ressources, et le client dans des conteneurs Docker séparés.

## Utilisation

- **Générer un token :** Utilisez la commande `make generate_and_save_token` pour générer un token d'authentification et le sauvegarder.
- **Vérifier un token :** Après avoir attendu 30 secondes pour simuler l'expiration du token, utilisez l'ID et le token sauvegardés pour vérifier l'authentification en exécutant `make verify_token`.

## Commandes Utiles

Le `Makefile` inclus dans le projet propose plusieurs commandes pour faciliter la gestion du projet :

- `make build` : Construit les images Docker.
- `make up` : Démarre tous les services.
- `make down` : Arrête tous les services et les supprime.
- `make remove` : Supprime tous les conteneurs et les images.
- `make logs` : Affiche les logs des services.
- `make run_client` : Exécute le client directement avec Go.
- `make generate_and_save_token` : Génère un nouveau token et le sauvegarde.
- `make verify_token` : Fait un test de vérification avec le dernier token valide sauvegardé. Assurez-vous d'attendre 30 secondes avant d'exécuter cette commande pour tester la réponse "token expired".
- `make verify_fake_token` : Teste la vérification avec un token inexistant.

## Nettoyage

Pour arrêter et supprimer les conteneurs, ainsi que les images Docker utilisées par le projet :

```bash
make down
make remove
```
